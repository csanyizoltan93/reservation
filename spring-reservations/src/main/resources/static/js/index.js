$(document).ready(() => {
	
  $('.bungalow-titles').on('mouseenter', event => {
    $(event.currentTarget).next().css('display' , 'inline');
  })
  $('.bungalow-titles').on('mouseleave', event => {
    $(event.currentTarget).next().css('display' , 'none');
  })
  
  $('.menu').on('mouseenter', event => {
    $(event.currentTarget).css('backgroundColor' , '#fcfa6a');
  })
  $('.menu').on('mouseleave', event => {
    $(event.currentTarget).css('backgroundColor' , '#e50202');
  })
  
});